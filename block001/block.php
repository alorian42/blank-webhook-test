<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__, $LANG);

$block = array(
    'code' => 'cnblock001',
    'fields' => array(
        'NAME'=> Loc::getMessage('CNBLOCK001_FIELDS_NAME', false, $LANG),
        'DESCRIPTION'=> Loc::getMessage('CNBLOCK001_FIELDS_NAME', false, $LANG),
        'SECTIONS'=>'text',
        'PREVIEW'=> 'https://'.$DOMAIN_SOURCE.$APP_URL.'blocks/block001/img/'.$LANG.'/preview.jpg',
        'CONTENT'=>'
        <section class="bar bar-3 bar-sm lateral-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bar__module">
                            <div class="type-fade header-title g-font-montserrat">'.Loc::getMessage('CNBLOCK001_FIELDS_CONTENT_TEXT1', false, $LANG).'</div>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end bar-->',
    ),
    'manifest' => array(
        "assets" => array(
            "css"=>array(                
                'https://'.$DOMAIN_SOURCE.$APP_URL.'blocks/sourse/css/theme.css',
				'https://'.$DOMAIN_SOURCE.$APP_URL.'blocks/sourse/css/fonts.css',
				'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,900&subset=cyrillic'
            )
        ),
        'nodes' => array(
            '.header-title' =>
            array(
                'name' => Loc::getMessage('CNBLOCK001_NODES_HEADER_TITLE_NAME', false, $LANG),
                'type' => 'text',
            ),
			
        ),
        'style' => array(
            'block' => array(
                'block-default',
            ),
            'nodes' => array(
                '.header-title' => array(
                    'name' => Loc::getMessage('CNBLOCK001_STYLE_NODES_HEADER_TITLE_NAME', false, $LANG),
                    'type' => 'typo',
                ),
				
            ),
        ),
    ),
);

return $block;
